# Flask from scrath

This is a course will be using Python, Flask and MySQL to create a simple web application from scratch.  
Here is the original course [Flask from scrath](https://code.tutsplus.com/series/creating-a-web-app-from-scratch-using-python-flask-and-mysql--cms-827)

In this project I'm lern about:

- Flask
- Python
- MySQL
- HTML
- CSS
- Boostrap
- Javascript.

This is a simple bucket list aplication where users can register, sign in and create their bucket list.
